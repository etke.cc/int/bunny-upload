module gitlab.com/etke.cc/tools/bunny-upload

go 1.21.0

toolchain go1.21.4

require (
	github.com/xxjwxc/gowp v0.0.0-20230612082025-23a9b62c1da6
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/xxjwxc/public v0.0.0-20240524110248-a4055cba2236 // indirect
	gopkg.in/eapache/queue.v1 v1.1.0 // indirect
)
